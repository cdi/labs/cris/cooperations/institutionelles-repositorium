#CRIS-DSpace interface for data transfer
* Create an interface that will allow the passing of Metadata and Bitstreams from CRIS to DSpace, but also the transfer of updated Metadata from DSpace to CRIS.
* The data transfer and synchronization between the systems should be done daily.


# DSpace 
*DSpace Documentation at https://wiki.lyrasis.org/display/DSDOC

## Overview
DSpace consists of a Java-based backend and an Angular-based frontend.
* Backend provides a REST API, along with other machine-based interfaces (e.g. OAI-PMH, SWORD, etc)
    * The entire REST Contract is at https://github.com/DSpace/RestContract
* Frontend is the User Interface built on the REST API
*Homepage at https://repotest.ub.fau.de (Testsystem, nur intern erreichbar)



#CRIS
*CRIS Documentation at https://cris.fau.de/docs

## Overview
The functionality of integrated current research information systems is based on the idea that already existing information can be linked together and maintained in this connected relationship. Maintaining linked information ought to simplify and harmonize research management processes for researchers and make administrative decision-making processes more transparent. This is guided by the fundamental idea to "record it once, use it repeatedly." 
